# ncspot-theme-generator

Create a theme for [ncspot](https://github.com/hrkfdn/ncspot) in your browser.

Try the tool:
https://ncspot-theme-generator.vaa.red

![gif animation of the tool](https://upload.vaa.red/i/VFfsK.gif)
