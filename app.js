let defaultColors = {
    background: '#181818',
    primary: '#f1f1f1',
    secondary: '#dcdcdc',
    title: '#1ed760',
    playing: '#1ed760',
    playing_selected: '#1ed760',
    playing_bg: '#181818',
    highlight: '#1db954',
    highlight_bg: 'black',
    error: '#FFE4E4',
    error_bg: '#A50000',
    statusbar: 'black',
    statusbar_progress: '#00ac4c',
    statusbar_bg: '#00ac4c',
    cmdline: '#cbffde',
    cmdline_bg: '#444444'
}

let userColors = {
    background: '',
    primary: '',
    secondary: '',
    title: '',
    playing: '',
    playing_selected: '',
    playing_bg: '',
    highlight: '',
    highlight_bg: '',
    error: '',
    error_bg: '',
    statusbar: '',
    statusbar_progress: '',
    statusbar_bg: '',
    cmdline: '',
    cmdline_bg: ''
}


function moveUp() {
    if ($('#tracks .track.highlight').prev().hasClass('track')) {
        updateCssColors()
        $('.highlight').removeClass('highlight').prev().addClass('highlight')
    }
}

function moveDown() {
    if ($('#tracks .track.highlight').next().hasClass('track')) {
        updateCssColors()
        $('.highlight').removeClass('highlight').next().addClass('highlight')
    }
}

function toggleError() {
    $('#error').toggle()
}

function toggleCmd() {
    $('#cmd').toggle()
}

function toggleSearch() {
    if ($('#search').is(':visible')) {
        $('#search').hide()
        $('#title').show()
    } else {
        $('#search').show()
        $('#title').hide()

    }
}

function updateConfigs() {
    let text = `[theme]
background = "` + background_pickr._color.toHEXA().toString() + `"
primary = "` + primary_pickr._color.toHEXA().toString() + `"
secondary = "` + secondary_pickr._color.toHEXA().toString() + `"
title = "` + title_pickr._color.toHEXA().toString() + `"
playing = "` + playing_pickr._color.toHEXA().toString() + `"
playing_selected = "` + playing_selected_pickr._color.toHEXA().toString() + `"
playing_bg = "` + playing_bg_pickr._color.toHEXA().toString() + `"
highlight = "` + highlight_pickr._color.toHEXA().toString() + `"
highlight_bg = "` + highlight_bg_pickr._color.toHEXA().toString() + `"
error = "` + error_pickr._color.toHEXA().toString() + `"
error_bg = "` + error_bg_pickr._color.toHEXA().toString() + `"
statusbar = "` + statusbar_pickr._color.toHEXA().toString() + `"
statusbar_progress = "` + statusbar_progress_pickr._color.toHEXA().toString() + `"
statusbar_bg = "` + statusbar_bg_pickr._color.toHEXA().toString() + `"
cmdline = "` + cmdline_pickr._color.toHEXA().toString() + `"
cmdline_bg = "` + cmdline_bg_pickr._color.toHEXA().toString() + `"
`
    $('#configs').text(text)
}

function updateCssColors() {
    let styles = `
        :root {
          --background: ` + background_pickr._color.toHEXA().toString() + `;
          --primary: ` + primary_pickr._color.toHEXA().toString() + `;
          --secondary: ` + secondary_pickr._color.toHEXA().toString() + `;
          --title: ` + title_pickr._color.toHEXA().toString() + `;
          --playing: ` + playing_pickr._color.toHEXA().toString() + `;
          --playing_selected: ` + playing_selected_pickr._color.toHEXA().toString() + `;
          --playing_bg: ` + playing_bg_pickr._color.toHEXA().toString() + `;
          --highlight: ` + highlight_pickr._color.toHEXA().toString() + `;
          --highlight_bg: ` + highlight_bg_pickr._color.toHEXA().toString() + `;
          --error: ` + error_pickr._color.toHEXA().toString() + `;
          --error_bg: ` + error_bg_pickr._color.toHEXA().toString() + `;
          --statusbar: ` + statusbar_pickr._color.toHEXA().toString() + `;
          --statusbar_progress: ` + statusbar_progress_pickr._color.toHEXA().toString() + `;
          --statusbar_bg: ` + statusbar_bg_pickr._color.toHEXA().toString() + `;
          --cmdline: ` + cmdline_pickr._color.toHEXA().toString() + `;
          --cmdline_bg: ` + cmdline_bg_pickr._color.toHEXA().toString() + `;
        }
      `
    $('#root').empty().text(styles)
}

document.onkeydown = function(e) {
    switch (e.which) {
        case 38:
            moveUp()
            break;

        case 40:
            moveDown()
            break;

        case 27:
            toggleError()
            break;

        case 89:
            toggleCmd()
            break;

        case 83:
            toggleSearch()
            break;
        default:
            return;
    }
    e.preventDefault()
}

var background_pickr = Pickr.create({
    el: '.color-picker-background',
    theme: 'nano',
    comparison: false,
    components: {
        preview: true,
        opacity: false,
        hue: true,
        interaction: {
            hex: false,
            rgba: false,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: true,
            clear: false,
            save: true
        }
    }
})

background_pickr.on('change', (color, instance) => {
    let rgb = color.toHEXA().toString()
    userColors.background = rgb
    updateConfigs()
    updateCssColors()
})

background_pickr.on('init', (instance) => {
    background_pickr.setColor(defaultColors.background)
})

var primary_pickr = Pickr.create({
    el: '.color-picker-primary',
    theme: 'nano',
    comparison: false,
    components: {
        preview: true,
        opacity: false,
        hue: true,
        interaction: {
            hex: false,
            rgba: false,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: true,
            clear: false,
            save: true
        }
    }
})

primary_pickr.on('change', (color, instance) => {
    let rgb = color.toHEXA().toString()
    userColors.primary = rgb
    updateConfigs()
    updateCssColors()
})

primary_pickr.on('init', (instance) => {
    primary_pickr.setColor(defaultColors.primary)
})


var secondary_pickr = Pickr.create({
    el: '.color-picker-secondary',
    theme: 'nano',
    comparison: false,
    components: {
        preview: true,
        opacity: false,
        hue: true,
        interaction: {
            hex: false,
            rgba: false,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: true,
            clear: false,
            save: true
        }
    }
})

secondary_pickr.on('change', (color, instance) => {
    let rgb = color.toHEXA().toString()
    userColors.secondary = rgb
    updateConfigs()
    updateCssColors()
})

secondary_pickr.on('init', (instance) => {
    secondary_pickr.setColor(defaultColors.secondary)
})

var title_pickr = Pickr.create({
    el: '.color-picker-title',
    theme: 'nano',
    comparison: false,
    components: {
        preview: true,
        opacity: false,
        hue: true,
        interaction: {
            hex: false,
            rgba: false,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: true,
            clear: false,
            save: true
        }
    }
})

title_pickr.on('change', (color, instance) => {
    let rgb = color.toHEXA().toString()
    userColors.title = rgb
    updateConfigs()
    updateCssColors()
})

title_pickr.on('init', (instance) => {
    title_pickr.setColor(defaultColors.title)
})

var playing_pickr = Pickr.create({
    el: '.color-picker-playing',
    theme: 'nano',
    comparison: false,
    components: {
        preview: true,
        opacity: false,
        hue: true,
        interaction: {
            hex: false,
            rgba: false,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: true,
            clear: false,
            save: true
        }
    }
})

playing_pickr.on('change', (color, instance) => {
    let rgb = color.toHEXA().toString()
    userColors.playing = rgb
    updateConfigs()
    updateCssColors()
})

playing_pickr.on('init', (instance) => {
    playing_pickr.setColor(defaultColors.playing)
})


var playing_selected_pickr = Pickr.create({
    el: '.color-picker-playing_selected',
    theme: 'nano',
    comparison: false,
    components: {
        preview: true,
        opacity: false,
        hue: true,
        interaction: {
            hex: false,
            rgba: false,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: true,
            clear: false,
            save: true
        }
    }
})

playing_selected_pickr.on('change', (color, instance) => {
    let rgb = color.toHEXA().toString()
    userColors.playing_selected = rgb
    updateConfigs()
    updateCssColors()
})

playing_selected_pickr.on('init', (instance) => {
    playing_selected_pickr.setColor(defaultColors.playing_selected)
})

var playing_bg_pickr = Pickr.create({
    el: '.color-picker-playing_bg',
    theme: 'nano',
    comparison: false,
    components: {
        preview: true,
        opacity: false,
        hue: true,
        interaction: {
            hex: false,
            rgba: false,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: true,
            clear: false,
            save: true
        }
    }
})

playing_bg_pickr.on('change', (color, instance) => {
    let rgb = color.toHEXA().toString()
    userColors.playing_bg = rgb
    updateConfigs()
    updateCssColors()
})

playing_bg_pickr.on('init', (instance) => {
    playing_bg_pickr.setColor(defaultColors.playing_bg)
})

var highlight_pickr = Pickr.create({
    el: '.color-picker-highlight',
    theme: 'nano',
    comparison: false,
    components: {
        preview: true,
        opacity: false,
        hue: true,
        interaction: {
            hex: false,
            rgba: false,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: true,
            clear: false,
            save: true
        }
    }
})

highlight_pickr.on('change', (color, instance) => {
    let rgb = color.toHEXA().toString()
    userColors.highlight = rgb
    updateConfigs()
    updateCssColors()
})

highlight_pickr.on('init', (instance) => {
    highlight_pickr.setColor(defaultColors.highlight)
})

var highlight_bg_pickr = Pickr.create({
    el: '.color-picker-highlight_bg',
    theme: 'nano',
    comparison: false,
    components: {
        preview: true,
        opacity: false,
        hue: true,
        interaction: {
            hex: false,
            rgba: false,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: true,
            clear: false,
            save: true
        }
    }
})

highlight_bg_pickr.on('change', (color, instance) => {
    let rgb = color.toHEXA().toString()
    userColors.highlight_bg = rgb
    updateConfigs()
    updateCssColors()
})

highlight_bg_pickr.on('init', (instance) => {
    highlight_bg_pickr.setColor(defaultColors.highlight_bg)
})

var error_pickr = Pickr.create({
    el: '.color-picker-error',
    theme: 'nano',
    comparison: false,
    components: {
        preview: true,
        opacity: false,
        hue: true,
        interaction: {
            hex: false,
            rgba: false,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: true,
            clear: false,
            save: true
        }
    }
})

error_pickr.on('change', (color, instance) => {
    let rgb = color.toHEXA().toString()
    userColors.error = rgb
    updateConfigs()
    updateCssColors()
})

error_pickr.on('init', (instance) => {
    error_pickr.setColor(defaultColors.error)
})

var error_bg_pickr = Pickr.create({
    el: '.color-picker-error_bg',
    theme: 'nano',
    comparison: false,
    components: {
        preview: true,
        opacity: false,
        hue: true,
        interaction: {
            hex: false,
            rgba: false,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: true,
            clear: false,
            save: true
        }
    }
})

error_bg_pickr.on('change', (color, instance) => {
    let rgb = color.toHEXA().toString()
    userColors.error_bg = rgb
    updateConfigs()
    updateCssColors()
})

error_bg_pickr.on('init', (instance) => {
    error_bg_pickr.setColor(defaultColors.error_bg)
})


var statusbar_pickr = Pickr.create({
    el: '.color-picker-statusbar',
    theme: 'nano',
    comparison: false,
    components: {
        preview: true,
        opacity: false,
        hue: true,
        interaction: {
            hex: false,
            rgba: false,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: true,
            clear: false,
            save: true
        }
    }
})

statusbar_pickr.on('change', (color, instance) => {
    let rgb = color.toHEXA().toString()
    userColors.statusbar = rgb
    updateConfigs()
    updateCssColors()
})

statusbar_pickr.on('init', (instance) => {
    statusbar_pickr.setColor(defaultColors.statusbar)
})

var statusbar_progress_pickr = Pickr.create({
    el: '.color-picker-statusbar_progress',
    theme: 'nano',
    comparison: false,
    components: {
        preview: true,
        opacity: false,
        hue: true,
        interaction: {
            hex: false,
            rgba: false,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: true,
            clear: false,
            save: true
        }
    }
})

statusbar_progress_pickr.on('change', (color, instance) => {
    let rgb = color.toHEXA().toString()
    userColors.statusbar_progress = rgb
    updateConfigs()
    updateCssColors()

})

statusbar_progress_pickr.on('init', (instance) => {
    statusbar_progress_pickr.setColor(defaultColors.statusbar_progress)
})


var statusbar_bg_pickr = Pickr.create({
    el: '.color-picker-statusbar_bg',
    theme: 'nano',
    comparison: false,
    components: {
        preview: true,
        opacity: false,
        hue: true,
        interaction: {
            hex: false,
            rgba: false,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: true,
            clear: false,
            save: true
        }
    }
})

statusbar_bg_pickr.on('change', (color, instance) => {
    let rgb = color.toHEXA().toString()
    userColors.statusbar_bg = rgb
    updateConfigs()
    updateCssColors()
})

statusbar_bg_pickr.on('init', (instance) => {
    statusbar_bg_pickr.setColor(defaultColors.statusbar_bg)
})



var cmdline_pickr = Pickr.create({
    el: '.color-picker-cmdline',
    theme: 'nano',
    comparison: false,
    components: {
        preview: true,
        opacity: false,
        hue: true,
        interaction: {
            hex: false,
            rgba: false,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: true,
            clear: false,
            save: true
        }
    }
})

cmdline_pickr.on('change', (color, instance) => {
    let rgb = color.toHEXA().toString()
    userColors.cmdline = rgb
    updateConfigs()
    updateCssColors()
})

cmdline_pickr.on('init', (instance) => {
    cmdline_pickr.setColor(defaultColors.cmdline)
})

var cmdline_bg_pickr = Pickr.create({
    el: '.color-picker-cmdline_bg',
    theme: 'nano',
    comparison: false,
    components: {
        preview: true,
        opacity: false,
        hue: true,
        interaction: {
            hex: false,
            rgba: false,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: true,
            clear: false,
            save: true
        }
    }
})

cmdline_bg_pickr.on('change', (color, instance) => {
    let rgb = color.toHEXA().toString()
    userColors.cmdline_bg = rgb
    updateConfigs()
    updateCssColors()
})

cmdline_bg_pickr.on('init', (instance) => {
    cmdline_bg_pickr.setColor(defaultColors.cmdline_bg)
})
